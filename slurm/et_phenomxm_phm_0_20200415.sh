#!/bin/bash
#SBATCH --job-name=et_phenxm_phm_0_20200415
#SBATCH --output=/fred/oz031/pbh_2021_logs/et_phenxm_phm_0_20200415_%A_%a.out
#SBATCH --ntasks=1
#SBATCH --time=0-23
#SBATCH --mem-per-cpu=3G
#SBATCH --tmp=4G
#SBATCH --array=0-63

source /fred/oz031/pbh_env/bin/activate

srun python /fred/oz031/3g-pbh-pe/scripts/run_inj_idx.py --injf /fred/oz031/3g-pbh-pe/injection_tables/injection_table_ET1_ET2_ET3_SNR12_20210415.txt --waveform IMRPhenomXPHM --ifo_list ET1 ET2 ET3 --outdir /fred/oz031/out_3g-pbh-pe/ET123_phenxm_phm_0_20210415/ --num $SLURM_ARRAY_TASK_ID
