#!/bin/bash
#SBATCH --job-name=ce_20200414
#SBATCH --output=/fred/oz031/pbh_2021_logs/ce_20200414_%A_%a.out
#SBATCH --ntasks=1
#SBATCH --time=0-3
#SBATCH --mem-per-cpu=3G
#SBATCH --tmp=4G
#SBATCH --array=0

source /fred/oz031/pbh_env/bin/activate

srun python /fred/oz031/3g-pbh-pe/scripts/bilby_run.py --m1src 2.50000 --m2src 2.500000 --theta_jn_deg 0.000000 --ra_deg 225.387954 --dec_deg -45.750606 --psi_deg 79.387176 --redshift 10.000000 --duration 128.000000 --fsample 2048.000000 --waveform IMRPhenomXPHM --fmin 4.000000 --fref 4.000000 --outdir /fred/oz031/CE_out/ --ifo_list CE
