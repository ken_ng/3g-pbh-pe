PE study for high redshift BBHs (Pop III or PBH) in a network of CE and ET detectors.

Bilby version: https://git.ligo.org/kwan-yeung.ng/bilby_3g (special courtesy to Rory Smith who built this version) This should be public. Let me know if you have permission issues.

Instructions:

1. An example command to run the Bilby script for PE ([./script/bilby_run.py](https://gitlab.com/ken_ng/3g-pbh-pe/-/blob/master/scripts/bilby_run.py))
`python bilby_run.py --m1src 2.50000 --m2src 2.500000 --theta_jn_deg 0.000000 --ra_deg 225.387954 --dec_deg -45.750606 --psi_deg 79.387176 --redshift 10.000000 --duration 128.000000 --fsample 2048.000000 --waveform IMRPhenomXPHM --fmin 4.000000 --fref 4.000000 --outdir ./CE_CES_ET1_ET2_ET3/ --ifo_list CE CES ET1 ET2 ET3`
All injection values of each network can be found in [./injection_tables/](https://gitlab.com/ken_ng/3g-pbh-pe/-/tree/master/injection_tables)
 
2. Organize the PE jobs into your computational system (condor or slurm). An example script to write a submit file for condor system can be found in [./scripts/write_submit.py](https://gitlab.com/ken_ng/3g-pbh-pe/-/blob/master/scripts/write_submit.py). This script compiles the commands of all injections of the same ifo-network and waveform under a single batch of condor submit file.

3. All PSDs and interferometer geometries are defaultly built in the above version of Bilby. Further changes can be made in *.interferometer files (in [./detectors/](https://gitlab.com/ken_ng/3g-pbh-pe/-/tree/master/detectors)), which can be recognized by Bilby using the customized options to replace the default ifos `new_ifo = bilby.gw.detector.load_interferometer("new_ifo.interferometer")`

