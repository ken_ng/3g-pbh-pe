import argparse
import pandas as pd

class ParFormats(object):
  """
  Translating command line parameter names to names used further on
  """
  def __init__(self):
    injpars = ['m1', 'm2', 'iota', 'ra', 'dec', 'psi', 'z', 'seglen', \
               'f_sample', 'f_low', 'f_ref']
    argspars = ['m1src','m2src','theta_jn_deg','ra_deg','dec_deg','psi_deg',\
                'redshift','duration','fsample','fmin','fref']
    self.pardict = dict(zip(injpars,argspars))

def parse_command_line():
  parser = argparse.ArgumentParser()
  parser.add_argument('--num',type=int,required=True,help='Injection index')
  parser.add_argument('--injf',type=str,required=True,help='Injection file')
  parser.add_argument('--m1src',type=float,default=0.,help='Mass 1 at source')
  parser.add_argument('--m2src',type=float,default=0.,help='Mass 2 at source')
  parser.add_argument('--theta_jn_deg',default=0.,type=float,help='thetajn,deg')
  parser.add_argument('--ra_deg',default=0.,type=float,help='ra,deg')
  parser.add_argument('--dec_deg',default=0.,type=float,help='dec,deg')
  parser.add_argument('--psi_deg',default=0.,type=float,help='psi,deg')
  parser.add_argument('--redshift',default=0.,type=float,help='redshift')
  parser.add_argument('--duration',default=0.,type=float,help='segment length')
  parser.add_argument('--fsample',default=0.,type=float,\
                      help='sampling frequency (2*f_high)')
  parser.add_argument('--fmin',default=0.,type=float,help='min frequency')
  parser.add_argument('--fref',default=0.,type=float,help='ref frequency')
  parser.add_argument('--waveform',required=True,type=str,help='waveform model')
  parser.add_argument('--outdir', type=str, default='./',help='output folder')
  parser.add_argument('--margPhase', default=False, action='store_true',\
                      help='use this flag for phase-marginalization')
  parser.add_argument('--seed', type=int, default=1290643798,\
                      help='seed for RNG')
  parser.add_argument("--ifo_list", nargs="+", required=True)
  
  return parser.parse_args()

def read_injf_with_pandas(args):
  """
  Reading injection table into Pandas data frame
  """
  with open(args.injf,'r') as injf:
    header = injf.readline()
  injtab = pd.read_csv(args.injf, names=header.replace('# ', '').split(), \
                       skiprows=1 ,delimiter=' ')
  return injtab.rename(mapper=ParFormats().pardict, axis='columns')

def populate_args_from_injf(args, verbose=True):
  injtab = read_injf_with_pandas(args)
  print('Populating arguments from an injection file, line ', args.num)
  for ag in args.__dict__.keys():
    if ag in injtab.keys():
      args.__dict__[ag] = injtab[ag][args.num]
      if verbose: print(ag, ': ', injtab[ag][args.num])
  if verbose:
    if args.margPhase:
      print('Marg Phase on')
    else:
      print('Marg Phase off')
  return args

def bilby_output_label(mtotal, q, redshift, theta_jn_deg, approx):
  return 'mtotal%.1f_q%.1f_z%.1f_iota%.1f_%s' %(mtotal, 1./q, redshift, \
          theta_jn_deg, approx)
