import numpy as np 
import bilby
import matplotlib.pyplot as plt
from scipy.stats import gaussian_kde
@outdir = "../public_html/"
@directory = "/home/kwan-yeung.ng/project/offline_pe/3G/PBH_PopIII/posteriors/uniform_Mtotal_Q/"
@waveforms = ["IMRPhenomXPHM","IMRPhenomPv2"]
@detector = "CE_ET1_ET2_ET3"
mtotals=[10.0,20.0,40.0,80.0,160.0,250.0]
qs = [1.0, 2.0, 5.0]
iotas = [0.0, 30.0, 60.0, 90.0]
zs = [10.0, 20.0, 30.0, 40.0]
for mtotal in mtotals:
  for q in qs:
    for z in zs:
      for iota in iotas:
        XPHM_json_file = directory+waveforms[0]+"/"+detector+"/"+"mtotal%.1f_q%.1f_z%.1f_iota%.1f_"%(mtotal, q,z, iota)+waveforms[0]+"_result.json"
        try:
          f = open("violinplots/"+detector+"/mtotal%.1f_q%.1f_z%.1f_iota%.1f_.txt"%(mtotal, q,z, iota),"w")
          XPHM_result = bilby.result.read_in_result(XPHM_json_file)
          qsamples = XPHM_result.posterior['mass_ratio'].values
          zsamples = XPHM_result.posterior['redshift'].values
          x_vec = np.linspace(zsamples.min(), zsamples.max())
          weights = qsamples**2/(1.+qsamples)/(1+zsamples)
          posterior_kde = gaussian_kde(zsamples, weights=weights)
          posterior_values = posterior_kde(x_vec)*15
          for i in x_vec:
            f.write(str(i))
            f.write(" ")
          f.write("\n")
          for i in posterior_values:
            f.write(str(i))
            f.write(" ")
          f.write("\n")
          XPHM_json_file = directory+waveforms[1]+"/"+detector+"/"+"mtotal%.1f_q%.1f_z%.1f_iota%.1f_"%(mtotal, q,z, iota)+waveforms[1]+"_result.json" 
          XPHM_result = bilby.result.read_in_result(XPHM_json_file)
          qsamples = XPHM_result.posterior['mass_ratio'].values
          zsamples = XPHM_result.posterior['redshift'].values
          x_vec = np.linspace(zsamples.min(), zsamples.max())
          weights = qsamples**2/(1.+qsamples)/(1+zsamples)
          posterior_kde = gaussian_kde(zsamples, weights=weights)
          posterior_values = posterior_kde(x_vec)*15
          for i in x_vec:
            f.write(str(i))
            f.write(" ")
          f.write("\n")
          for i in posterior_values:
            f.write(str(i))
            f.write(" ")
          f.write("\n")
          f.close()
        except:
          pass

