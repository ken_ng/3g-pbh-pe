import numpy as np
import os

# specify ifo network: "CE" for CE in US, "CES" for CE in Australia, "ET1", "ET2", "ET3" for the full ET
ifo_list=["CE","CES","ET1","ET2","ET3"] #CE*2
# generate necessary strings for I/O purpose
ifo_list_tag=""
ifo_list_parse=""
for ifo in ifo_list:
	ifo_list_tag += "%s_" %ifo
	ifo_list_parse += "%s " %ifo
ifo_list_tag=ifo_list_tag[:-1]
# add extra strings to distinguish different set of runs, if you've changed something in the script or parser arguments
extra_tag =""
outdir_tag = ifo_list_tag+extra_tag
logdir_tag = ifo_list_tag+extra_tag
submit_tag = ifo_list_tag+extra_tag
# load injection values
inputs = np.genfromtxt("injection_table_%s_SNR12.txt" %ifo_list_tag, names=True)
# make sure output and log directories exist
outdir="/home/kwan-yeung.ng/project/offline_pe/3G/PBH_PopIII/%s/" %outdir_tag
logdir="/home/kwan-yeung.ng/project/offline_pe/3G/PBH_PopIII/%s/logs/" %logdir_tag
bilby_file = "/home/kwan-yeung.ng/project/offline_pe/3G/PBH_PopIII/bilby_run.py"
pythonpath = "/home/kwan-yeung.ng/.conda/envs/bilby_3g/bin/python3"
if not os.path.exists(logdir):
	os.makedirs(logdir)

# specify waveform approx: for us, IMRPhenomXPHM or IMRPhenomPv2
approx = "IMRPhenomPv2"
# for IMRPhenomPv2, we will use phase-marginalization
if approx=="IMRPhenomPv2":
	marg_flag = "--margPhase"
	submit_tag+="_margPhase"
else:	marg_flag = ""

# write submit file
# change the accounting group information accordingly.
submit=\
"""universe = vanilla
getenv = true
executable = %s
output = %s/$(cluster).$(process).out
error = %s/$(cluster).$(process).err
accounting_group = ligo.dev.o3.cbc.pe.lalinference
accounting_group_user = kwan-yeung.ng

"""%(pythonpath, logdir, logdir)

for (m1_src, m2_src, theta_jn_deg, ra_deg, dec_deg, psi_deg, z, seglen, f_sample, f_low, f_ref) in zip(inputs['m1'], inputs['m2'], inputs['iota'], inputs['ra'], inputs['dec'], inputs['psi'], inputs['z'], inputs['seglen'], inputs['f_sample'], inputs['f_low'], inputs['f_ref']):
	label = '%d_%d_%d_%s' %(m1_src, m2_src, z, approx)
	submit+=\
"""
arguments = %s --m1src %f --m2src %f --theta_jn_deg %f --ra_deg %f --dec_deg %f --psi_deg %f --redshift %f --duration %f --fsample %f --waveform %s --fmin %f --fref %f --outdir %s --ifo_list %s %s
queue

""" %(bilby_file, m1_src, m2_src, theta_jn_deg, ra_deg, dec_deg, psi_deg, z, seglen, f_sample, approx, f_low, f_ref, outdir, ifo_list_parse, marg_flag)

# save submit
with open("%s_%s.submit" %(approx,submit_tag),'w') as sub_file:
	sub_file.write(submit)
