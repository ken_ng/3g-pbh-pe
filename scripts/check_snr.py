from __future__ import division, print_function
import os
import sys
sys.path.append('/home/kwan-yeung.ng/gitlab/bilby_rory_smith')
import numpy as np
import bilby
import lalsimulation as lalsim
from lal import PC_SI, MSUN_SI, MTSUN_SI

def inner_prod(freq, h_f, S_n):
	return 4*np.trapz(np.abs(h_f)**2/S_n, x=freq)

def inspiral_time_interval(f_final, f_start, m1, m2):
	mchirp=(m1*m2)**(3./5.)/(m1+m2)**(1./5.)
	return 5./256./np.pi**(8./3.)/(MTSUN_SI*mchirp)**(5./3.)*(1./f_start**(8./3.)-1./f_final**(8./3.))

def get_f_isco(m1,m2):
	return 1./6**(1.5)/np.pi/(m1+m2)/Msun_s

def get_f_RD(m1,m2):
	return 1207.*10./(m1+m2)

cosmo = bilby.gw.cosmology.DEFAULT_COSMOLOGY
mtotals = [5.0, 10.0, 20.0, 40.0, 80.0, 160.0, 250.0]
qs = [1.0, 0.5, 0.2]
iotas = [0.,np.pi/3.0,np.pi/2.0]
zs = [40.0, 30.0, 20.0, 10.0]
gps_time=1577491218.0
N=10000
ras = np.random.uniform(0.,2*np.pi, N)
decs = np.pi/2.0 - np.arccos(np.random.uniform(-1.0,1.0, N))
psis = np.random.uniform(0.,2*np.pi,N)
ifos = ['CE','CES']#,'ET1','ET2','ET3']
for ifo in ifos:
	exec("%s = bilby.gw.detector.load_interferometer('%s_lf.interferometer')" %(ifo,ifo))
	exec("fps_%s, fcs_%s=[],[]" %(ifo,ifo))
	exec(
	"""for (ra, dec, psi) in zip(ras, decs, psis):
	fp_%s = %s.antenna_response(ra, dec, gps_time, psi, 'plus', 500, np.array([8]))[0]
	fc_%s = %s.antenna_response(ra, dec, gps_time, psi, 'cross', 500, np.array([8]))[0]
	fps_%s.append(fp_%s)
	fcs_%s.append(fc_%s)
	""" %(ifo,ifo,ifo,ifo,ifo,ifo,ifo,ifo)
	)

headers = "#m1 m2 iota ra dec psi z seglen f_sample f_low f_ref"
inj_fname = "injection_table"
for ifo in ifos:
	headers+=" rho_%s" %ifo
	inj_fname+="_%s" %ifo
headers+=" rho_net"
inj_fname+=".txt"
with open(inj_fname,'w') as inj_table:
	inj_table.write(headers+'\n')

df = 1./16.
for mtotal in mtotals:
	for q in qs:
		for z in zs:
			for iota in iotas:
				m1z = mtotal/(1+q)*(1+z)
				m2z = mtotal/(1/q+1)*(1+z)
				mchirpz = (m1z*m2z)**(3./5.)/(m1z+m2z)**(1./5.)
				fcut = 0.2/(5e-6*2*(m1z+m2z))
				flow = min(4.0, 2**np.floor(np.log(fcut)/np.log(2.0)))
				fRD = get_f_RD(m1z, m2z)
				fhigh = 2**np.ceil(np.log(fRD)/np.log(2.0))*4
				fhigh = max(128.0,fhigh)
				length = inspiral_time_interval(fhigh, flow, m1z, m2z)
				if length+2>2:
					seglength = 2**np.ceil(np.log(length+2.0)/np.log(2.0))
				else:
					seglength=4.0
				fref = flow
				dL = cosmo.luminosity_distance(z).value*1e6
				hp, hc = lalsim.SimInspiralFD(m1z*MSUN_SI, m2z*MSUN_SI, 0., 0., 0., 0., 0., 0., dL*PC_SI, iota, 0., 0., 0., 0., df, flow, fhigh, fref, None, lalsim.IMRPhenomPv2)
				freqs = np.arange(0., fhigh+df, df)
				f_select = (freqs>=flow)*(freqs<=fhigh)
				rho_tmps_net = np.zeros(N)
				for ifo in ifos:
					exec("rho_tmps_%s=np.zeros(N)" %ifo)
					exec("S_n_%s = %s.power_spectral_density.get_power_spectral_density_array(freqs[f_select])" %(ifo,ifo))
					exec("""for (i,(fp, fc)) in enumerate(zip(fps_%s, fcs_%s)):
		h_f = hp.data.data[f_select]*fp+hc.data.data[f_select]*fc
		rhosq_tmp = inner_prod(freqs[f_select], h_f, S_n_%s)
		rho_tmps_%s[i]= np.sqrt(rhosq_tmp)""" %(ifo,ifo,ifo,ifo))
					exec("rho_tmps_net=np.sqrt(rho_tmps_net**2+rho_tmps_%s**2)" %ifo)
				idx = rho_tmps_net.argmax()
				inj_string="%f %f %f %f %f %f %f %f %f %f %f " %(m1z/(1+z), m2z/(1+z), iota*180/np.pi, ras[idx]*180/np.pi, decs[idx]*180/np.pi, psis[idx]*180/np.pi, z, seglength, fhigh*2.0, flow, fref)
				for ifo in ifos:
					exec("inj_string+='{} '.format(rho_tmps_%s[idx])" %ifo)
				inj_string+="%f\n" %rho_tmps_net[idx]
				with open(inj_fname,'a') as inj_table:
					inj_table.write(inj_string)
