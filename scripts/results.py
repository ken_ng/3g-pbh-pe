"""
Processing the results for multiple injections.

Usage example:
python results.py --injf /fred/oz031/3g-pbh-pe/injection_tables/injection_table_ET1_ET2_ET3_SNR12_20210415.txt --waveform IMRPhenomPv2 --ifo_list ET1 ET2 ET3 --margPhase --outdir /fred/oz031/out_3g-pbh-pe/ET123_phenpv2_phm_1_20210415/ --num -1
"""

import os
import bilby
import numpy as np
import core_inj_idx as core
from matplotlib import pyplot as plt

make_pp_plot = False
plot_corner = False

load_result = True if (plot_corner or make_pp_plot) else False

args = core.parse_command_line()
injtab = core.read_injf_with_pandas(args)
# Use --num -1 to loop through all results
if args.num != -1:
  injf_lines = [args.num]
else:
  injf_lines = [ii for ii in range(len(injtab.index))]

print('Checking the status of ', len(injf_lines), ' injections:')
results = []
completed = []
need_to_continue = []
need_to_start = []
i_m1src, i_m2src, i_redshift, i_theta_jn_deg = [], [], [], []
r_z, r_zerr, r_dl, r_dlerr = [], [], [], []
for ll in injf_lines:
  args.num = ll
  args  = core.populate_args_from_injf(args, verbose=False)
  [x.append(y) for x, y in zip([i_m1src, i_m2src, i_redshift, i_theta_jn_deg], \
                   [args.m1src, args.m2src, args.redshift, args.theta_jn_deg])]
  label = core.bilby_output_label(args.m1src+args.m2src, args.m2src/args.m1src,\
                                  args.redshift, args.theta_jn_deg, \
                                  args.waveform)

  basename = args.outdir + '/' + label
  if os.path.isfile(basename + '_result.json'):
    print(ll,' completed')
    if load_result:
      results.append(bilby.result.read_in_result(filename=basename + \
                                                          '_result.json'))
      r_z.append( np.median(results[-1].posterior['redshift']) )
      r_dl.append( np.median(results[-1].posterior['luminosity_distance']) )
      r_zerr.append( np.percentile(results[-1].posterior['redshift'], 84, axis=0) - np.percentile(results[-1].posterior['redshift'], 16, axis=0) )
      r_dlerr.append( np.percentile(results[-1].posterior['luminosity_distance'], 84, axis=0) - np.percentile(results[-1].posterior['luminosity_distance'], 16, axis=0) )
    if plot_corner:
      results[-1].plot_corner()
  elif os.path.isfile(basename + '_resume.pickle'):
    print(ll, ' is incomplete')
    need_to_continue.append(ll)
  else:
    print(ll, ' has not produced any checkpoints')
    need_to_start.append(ll)

i_m1src, i_m2src, i_redshift, i_theta_jn_deg = np.array(i_m1src), np.array(i_m2src), np.array(i_redshift), np.array(i_theta_jn_deg)

if make_pp_plot:
  print('========================')
  pp_name = args.outdir+'pp.png'
  bilby.result.make_pp_plot(results)
  plt.savefig(pp_name)
  plt.close()
  print('PP plot completed: ', pp_name)

print('========================')
print('Total injections: ', len(injf_lines))
print('Completed: ', len(completed))
print('Checkpoint files: ', len(need_to_continue))
print('No checkpoint files: ', len(need_to_start))

if need_to_continue != []:
  print('\n To restart: ', ','.join([str(nn) for nn in need_to_continue]))

plt.hist(i_m2src/i_m1src,label='q',bins=10)
plt.xlabel('Injected mass ratio')
plt.ylabel('Number of injections')
plt.savefig(args.outdir + 'inj_q.png')
plt.close()

plt.hist(i_m2src+i_m1src,label='Mtot',bins=10)
plt.xlabel('Injected total mass [Ms]')
plt.ylabel('Number of injections')
plt.savefig(args.outdir + 'inj_mtot.png')
plt.close()

cosmo = bilby.gw.cosmology.DEFAULT_COSMOLOGY

plt.hist(cosmo.luminosity_distance(i_redshift).value, label='dL inj',bins=10,alpha=0.5)
plt.hist(r_dl, label='dL rec',bins=10,alpha=0.5)
plt.xlabel('Luminosity distance [kpc]')
plt.ylabel('Number of injections')
plt.legend()
plt.savefig(args.outdir + 'inj_rec_dl.png')
plt.close()

if load_result:
  # dlerr
  plt.hist(r_dlerr, label='dL err', bins=10)
  plt.xlabel('Luminosity distance error 16-84 CDF [kpc]')
  plt.ylabel('Number of injections')
  plt.savefig(args.outdir + 'rec_dl_err.png')
  plt.close()

  # zerr(z)
  zz = [val for ii, val in enumerate(i_redshift) if ii not in need_to_continue]
  plt.scatter(r_zerr, r_z, label='z rec')
  plt.scatter(r_zerr, zz, label='z inj')
  plt.xlabel('Recovered z err')
  plt.ylabel('z')
  plt.legend()
  plt.savefig(args.outdir + 'inj_z_rec_z_z_err.png')
  plt.close()

import ipdb; ipdb.set_trace()
