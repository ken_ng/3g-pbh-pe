import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d
from scipy.integrate import cumtrapz
import numpy as np
@outdir = "../public_html/violinplots/q_vs_z/"
@directory = "/home/kwan-yeung.ng/project/offline_pe/3G/PBH_PopIII/posteriors/uniform_Mtotal_Q/"
@waveforms = ["IMRPhenomXPHM","IMRPhenomPv2"]
@detector = "CE_ET1_ET2_ET3"
mtotals=[10.0,20.0,40.0,80.0,160.0,250.0]
qs = [1.0, 2.0, 5.0]
iotas = [0.0, 30.0, 60.0, 90.0]
zs = [10.0, 20.0, 30.0, 40.0]
for mtotal in mtotals:
  for iota in iotas:
    for z in zs:
      for q in qs:
        resultfile = "violinplots/"+detector+"/"+"mtotal%.1f_q%.1f_z%.1f_iota%.1f_"%(mtotal, q,z, iota)+".txt"
        try:
          f = open(resultfile,'r')
          firstlineX = f.readline().split()
          x_vecX = []
          for i in firstlineX:
            x_vecX.append(float(i))
          x_vecX = np.array(x_vecX)
          secondlineX = f.readline().split()
          posterior_valuesX = []
          for i in secondlineX:
            posterior_valuesX.append(float(i))
          posterior_valuesX = np.array(posterior_valuesX)
          offset = q # the reference x-coordinate on the plot                                                         
          #plt.fill_betweenx(x_vecX, offset-posterior_valuesX, offset, color = 'b')
          cumulative_distribution_valuesX = cumtrapz(posterior_valuesX, x=x_vecX, initial=0)
          cumulative_distribution_valuesX /= cumulative_distribution_valuesX [-1] # this is to make sure the cumulative function actually goes from 0 to 1
          percent_point_functionX = interp1d(cumulative_distribution_valuesX, x_vecX) # this constructs the inverse function of cumulative distribution function
          # Let's do 95\% (2-sigma correspondence to a gaussian)
          Lower_boundX = percent_point_functionX(0.025)
          Upper_boundX = percent_point_functionX(0.975)
          plt.plot([offset, offset - 0.5], [Lower_boundX, Lower_boundX], c='k')
          plt.plot([offset, offset - 0.5], [Upper_boundX, Upper_boundX], c='k')
          firstline = f.readline().split()
          x_vec = []
          for i in firstline:
            x_vec.append(float(i))
          x_vec = np.array(x_vec)
          secondline = f.readline().split()
          posterior_values = []
          for i in secondline:
            posterior_values.append(float(i))
          posterior_values = np.array(posterior_values)
          highest = max(posterior_values.max(),posterior_valuesX.max())
          posterior_values = 0.5/highest*posterior_values
          posterior_valuesX = 0.5/highest*posterior_valuesX
          plt.fill_betweenx(x_vecX, offset-posterior_valuesX, offset, color = 'b')
          plt.fill_betweenx(x_vec, offset+posterior_values, offset, color = 'r')
          cumulative_distribution_values = cumtrapz(posterior_values, x=x_vec, initial=0)
          cumulative_distribution_values /= cumulative_distribution_values [-1] # this is to make sure the cumulative function actually goes from 0 to 1                
          percent_point_function = interp1d(cumulative_distribution_values, x_vec) # this constructs the inverse function of cumulative distribution function        
          # Let's do 95\% (2-sigma correspondence to a gaussian)                                                                                                     
          Lower_bound = percent_point_function(0.025)
          Upper_bound = percent_point_function(0.975)
          plt.plot([offset, offset + 0.5], [Lower_bound, Lower_bound], c='k')
          plt.plot([offset, offset + 0.5], [Upper_bound, Upper_bound], c='k')
          if len(x_vec)>0:
            plt.hlines(y = z, xmin = offset - 1, xmax = offset +1, linestyles = 'dashed')
        except:
          pass
      plt.xlabel("q")
      plt.ylabel("z")
      plt.title(detector+"_"+"mtotal%.1f_iota%.1f_z%.1f_"%(mtotal, iota,z))
      red_patch = mpatches.Patch(color='red', label="IMRPhenomPv2")
      #plt.legend(handles=[red_patch])
      blue_patch = mpatches.Patch(color='blue', label="IMRPhenomXPHM")
      plt.legend(handles=[red_patch,blue_patch])
      plt.savefig(outdir+detector+"/"+detector+"_mtotal"+str(mtotal)+"_iota"+str(iota)+"_z"+str(z)+".jpg")
      plt.close()
